﻿using UnityEngine;
using System.Collections;

public class Windcreate : MonoBehaviour {
	public GameObject WindUp;		//上
	public GameObject WindDown;		//下
	public GameObject WindRigth;	//右
	public GameObject WindLeft;		//左

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		// 上向きの風を作る
		if(Input.GetKeyDown(KeyCode.Q)){
			// ゲームオブジェクトを複製
			// 戻り値はObjectクラスなので、必ず複製したオブジェクトと同じクラスへキャストする
			GameObject instance = (GameObject)Instantiate(WindUp);
			// 複製したオブジェクトの位置をemmitterオブジェクトと同じ位置へ
			instance.transform.position = gameObject.transform.position;
			// 色をランダムで変更
			//switch(Random.Range(0,3)){
			//case 0: instance.renderer.material.color = Color.red; break;
			//case 1: instance.renderer.material.color = Color.blue; break;
			//case 2: instance.renderer.material.color = Color.green; break;
			//case 3: instance.renderer.material.color = Color.magenta; break;
			//}
		}
		//下向きの風を作る
		if (Input.GetKeyDown (KeyCode.W)) {
			// ゲームオブジェクトを複製
			// 戻り値はObjectクラスなので、必ず複製したオブジェクトと同じクラスへキャストする
			GameObject instance = (GameObject)Instantiate(WindDown);
			// 複製したオブジェクトの位置をemmitterオブジェクトと同じ位置へ
			instance.transform.position = gameObject.transform.position;
			// 色をランダムで変更
			//switch(Random.Range(0,3)){
			//case 0: instance.renderer.material.color = Color.red; break;
			//case 1: instance.renderer.material.color = Color.blue; break;
			//case 2: instance.renderer.material.color = Color.green; break;
			//case 3: instance.renderer.material.color = Color.magenta; break;
			//}
		}
		//左向きの風を作る
		if (Input.GetKeyDown (KeyCode.E)) {
			// ゲームオブジェクトを複製
			// 戻り値はObjectクラスなので、必ず複製したオブジェクトと同じクラスへキャストする
			GameObject instance = (GameObject)Instantiate(WindLeft);
			// 複製したオブジェクトの位置をemmitterオブジェクトと同じ位置へ
			instance.transform.position = gameObject.transform.position;
			// 色をランダムで変更
			//switch(Random.Range(0,3)){
			//case 0: instance.renderer.material.color = Color.red; break;
			//case 1: instance.renderer.material.color = Color.blue; break;
			//case 2: instance.renderer.material.color = Color.green; break;
			//case 3: instance.renderer.material.color = Color.magenta; break;
			//}
		}
		//右向きの風を作る
		if (Input.GetKeyDown (KeyCode.R)) {
			// ゲームオブジェクトを複製
			// 戻り値はObjectクラスなので、必ず複製したオブジェクトと同じクラスへキャストする
			GameObject instance = (GameObject)Instantiate(WindRigth);
			// 複製したオブジェクトの位置をemmitterオブジェクトと同じ位置へ
			instance.transform.position = gameObject.transform.position;
			// 色をランダムで変更
			//switch(Random.Range(0,3)){
			//case 0: instance.renderer.material.color = Color.red; break;
			//case 1: instance.renderer.material.color = Color.blue; break;
			//case 2: instance.renderer.material.color = Color.green; break;
			//case 3: instance.renderer.material.color = Color.magenta; break;
			//}
		}
	}
}
