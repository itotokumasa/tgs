﻿using UnityEngine;
using System.Collections;

public class Move : MonoBehaviour {
	[SerializeField, HideInInspector]SpriteRenderer spriteRenderer;
	[SerializeField, HideInInspector]Rigidbody2D rig2d;//リジットボディの取得
	//Savepointのオブジェクトを読んでおく
	public GameObject sp;

	//変数
	public float speed = 3;//スピード
	private int JumpCnt = 0;
	public float ResX = 0, ResY = 5;//初期リスポーン位置

	// Use this for initialization
	void Start () {
		sp = GameObject.Find("SavePoint");//これ消したらリスポーン処理がおかしくなるよ

	}
		
	void Awake(){
		spriteRenderer = GetComponent<SpriteRenderer> ();
		rig2d = GetComponent<Rigidbody2D> ();//リジットボディ
	}
	
	// Update is called once per frame
	void Update () {
		//入力動作を取得
		float axis = Input.GetAxisRaw ("Horizontal");//左右の入力
		bool isDown = Input.GetAxisRaw ("Vertical") < 0;//上下の入力
		//デバッグ
		//Debug.Log (axis);


	
		//移動系ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
	
		//ジャンプ回数は２回まで
		if (JumpCnt < 2) {
			//ジャンプボタンが押されたら
			if (Input.GetButtonDown ("Jump") || Input.GetKeyDown (KeyCode.W)) {
				//処理
				Jump();
			}
		}
	
		//左か右を入力したら
		//移動処理
		if(axis != 0)//1:右　-1:左
		{
			//入力方向へ移動
			rig2d.velocity = new Vector2(axis * speed, rig2d.velocity.y);

			//Wait→Dash
			//画像反転した
			spriteRenderer.flipX = -axis < 0;
		}
		else//0の場合、何も押されていない
		{
			//左右共操作がなかったら横移動の速度を0にしてピタッと止まるようにする
			rig2d.velocity = new Vector2(0, rig2d.velocity.y);
		}
		//ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー

		//各操作処理ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー
		if (Input.GetKey (KeyCode.Y)) {//リスポーンします
			respawn (ResX, ResY);//セーブポイントに移動

		} else if (Input.GetKey (KeyCode.M)) {//メニュー開くよ
			//Scene移動はせずにタイマーを止めて、メニューを表示

		}
		//ーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーーー

	}




	//ジャンプ処理
	public void Jump(){
		//力のベクトルの指定
		rig2d.velocity = transform.up * 5;//5ずつ上昇させとく
		//ジャンプさせたらカウントを増やす
		JumpCnt++;
	}
	//リスポーン:x,yは復活場所の位置
	void respawn(float x, float y){
		//位置をリスポーン位置に変更
		transform.position = new Vector2 (x, y);
	}
	//セーブポイントに触れたら,リスポーン位置を取得
	float getResPosX( GameObject Sp ){
		float x;
		//セーブポイント,Xの位置を取得
		x = Sp.transform.position.x;
		return x;
	}
	float getResPosY( GameObject Sp ){
		float y;
		//セーブポイント,Yの位置を取得
		y = Sp.transform.position.y;
		return y;
	}

	//当たり判定
	void OnCollisionEnter2D(Collision2D coll){
		//地面との接触
		if (coll.gameObject.tag == "Ground") {
			JumpCnt = 0;
		}else if(coll.gameObject.tag == "SavePoint"){//セーブポイントと接触した場合
			//セーブポイントの位置を取得
			ResX = getResPosX(sp);//x座標
			ResY = getResPosY(sp);//y座標

			//デバッグ
			Debug.Log("セーブしました");
		}


	}
}
