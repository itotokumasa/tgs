﻿using UnityEngine;
using System.Collections;

public class BattleSpriteAction : MonoBehaviour
{
	static int hashSpeed = Animator.StringToHash ("Speed");
	static int hashFallSpeed = Animator.StringToHash ("FallSpeed");
	static int hashGroundDistance = Animator.StringToHash ("GroundDistance");
	static int hashIsCrouch = Animator.StringToHash ("IsCrouch");
	static int hashAttack1 = Animator.StringToHash ("Attack1");
	static int hashAttack2 = Animator.StringToHash ("Attack2");
	static int hashAttack3 = Animator.StringToHash ("Attack3");


	static int hashDamage = Animator.StringToHash ("Damage");
	static int hashIsDead = Animator.StringToHash ("IsDead");

	[SerializeField] private float characterHeightOffset = 0.2f;
	[SerializeField] LayerMask groundMask;

	[SerializeField, HideInInspector] Animator animator;
	[SerializeField, HideInInspector]SpriteRenderer spriteRenderer;
	[SerializeField, HideInInspector]Rigidbody2D rig2d;

	public int hp = 4;



	void Awake ()
	{
		animator = GetComponent<Animator> ();
		spriteRenderer = GetComponent<SpriteRenderer> ();
		rig2d = GetComponent<Rigidbody2D> ();
	}

	void Update ()
	{
        //入力動作を取得
		float axis = Input.GetAxisRaw ("Horizontal");
		bool isDown = Input.GetAxisRaw ("Vertical") < 0;

        //ジャンプボタンが押されたら
        if (Input.GetButtonDown("Jump") || Input.GetKeyDown(KeyCode.W))
        {
			rig2d.velocity = new Vector2 (rig2d.velocity.x, 5);
		}
     
        //左か右を入力したら
        if (axis != 0)
        {
            //入力方向へ移動
            rig2d.velocity = new Vector2(axis * 3f, rig2d.velocity.y);
 
            //Wait→Dash
            spriteRenderer.flipX = axis < 0;
        }
        else
        {
            //左右共操作がなかったら横移動の速度を0にしてピタッと止まるようにする
            rig2d.velocity = new Vector2(0, rig2d.velocity.y);
        }

		var distanceFromGround = Physics2D.Raycast (transform.position, Vector3.down, 1, groundMask);

		// update animator parameters
		animator.SetBool (hashIsCrouch, isDown);
		animator.SetFloat (hashGroundDistance, distanceFromGround.distance == 0 ? 99 : distanceFromGround.distance - characterHeightOffset);
		animator.SetFloat (hashFallSpeed, rig2d.velocity.y);
		animator.SetFloat (hashSpeed, Mathf.Abs (axis));
		if( Input.GetKeyDown(KeyCode.Z) ){  animator.SetTrigger(hashAttack1); }
		if( Input.GetKeyDown(KeyCode.X) ){  animator.SetTrigger(hashAttack2); }
		if( Input.GetKeyDown(KeyCode.C) ){  animator.SetTrigger(hashAttack3); }

		// flip sprite
		if (axis != 0)
			spriteRenderer.flipX = axis < 0;
	}
}
